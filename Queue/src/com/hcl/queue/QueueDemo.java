package com.hcl.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueDemo {
	
	public static void main(String[] args) {
		
		
		Queue<Integer> que = new PriorityQueue<Integer>(15,new MyComparator());
		
		que.add(4);
		que.add(6);
		que.add(1);
		que.add(9);
		que.add(45);
		que.add(1);
		System.out.println(que);
		
		
		System.out.println(que.poll());
		System.out.println(que);
		System.out.println(que.peek());
		
		
		
		
		
	}

}
