package com.hcl.day1;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;
import java.util.Stack;

public class VectorDemo {

	public static void main(String[] args) {
		
		
		Vector<String> list = new Vector<String>();
		   
		   
        list.addElement("Nishi");
        list.add("Vidyasri");
        list.add(0, "Mani");
        list.add(null);
        
        System.out.println(list);
        
     Iterator it = list.iterator();
     
     Enumeration<String> enu = list.elements();
     ListIterator<String> listIt = list.listIterator();
     
     while (listIt.hasNext()) {
   	  String str = (String) listIt.next();
   	  System.out.println(str);
   	  
     }
      while(enu.hasMoreElements()) {
   	   String str2 = (String)enu.nextElement();
   	   System.out.println(str2);
      }
  
		
		       
		Stack<Integer> stack = new Stack<Integer>();
		
		      stack.add(99);
		      stack.add(55);
		      stack.push(400);
		      stack.addElement(100);
		      System.out.println(stack);
		      System.out.println(stack.peek());

		      System.out.println(stack.search(100));
		      System.out.println(stack.pop());
		      System.out.println(stack);
		      
		    
		       
		

	}

}
