package com.hcl.Day1;

import java.util.Iterator;
import java.util.Stack;

public class StackImplementation {

  

    public static void main(String args[]) 

    { 

        Stack <String> stack = new Stack<>();
    	// Create an object of Stack of type String
        
        stack.push("Nishi");
        stack.push("Mani");
        stack.push("Vidya");
        stack.push("Mouni");
        
        System.out.println(stack);
        Iterator value = stack.iterator();
        
        // Displaying the values
        // after iterating through the stack
        System.out.println("The iterator values are: ");
        while (value.hasNext()) {
            System.out.println(value.next());
        }

        
    }

        

        // Display the values using the iterator 

        

      

}

