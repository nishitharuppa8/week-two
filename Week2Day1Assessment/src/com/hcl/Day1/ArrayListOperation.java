package com.hcl.Day1;
import java.util.ArrayList;
import java.util.Collections;
 
class GFG {
    public static void main(String[] args)
    {
        ArrayList<Integer> a1 = new ArrayList<>();
        a1.add(10);
        a1.add(25);
        a1.add(43);
        a1.add(50);
        a1.add(70);
        System.out.println(a1);
        System.out.println("Size of the elements: " +a1.size());
        
        a1.remove(0);
        a1.remove(1);
        System.out.println(a1);
        System.out.println(Collections.min(a1));
        System.out.println(Collections.max(a1));
        
        for (int i = 0; i < a1.size(); i++) {
            System.out.println(a1.get(i) + " ");
        }
        
    }
}