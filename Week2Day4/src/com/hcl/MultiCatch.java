package com.hcl;

public class MultiCatch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = new int[5];
		try {
			System.out.println(a[10]);
		}
		catch(ArithmeticException e) {
			System.err.println("Arithmetic");
		}
		catch(ArrayIndexOutOfBoundsException ab) {
			System.err.println("Array");
		}
		catch(Exception ex) {
			System.out.println("Other");
		}
		System.out.println("Out Of Block");

	}

}
