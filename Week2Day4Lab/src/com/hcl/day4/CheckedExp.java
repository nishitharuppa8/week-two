package com.hcl.day4;


import java.io.FileNotFoundException;
import java.io.FileReader;

public class CheckedExp {

	public static void main(String[] args)  {
		// TODO Auto-generated method stub
		
		FileReader reader = null;
		try {
			reader = new FileReader("src/hello.txt");
		} catch (FileNotFoundException e) {
			
			
			System.err.println("File not found");
			
		}

		System.out.println(reader);
		
	}

}
