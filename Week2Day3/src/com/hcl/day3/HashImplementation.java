package com.hcl.day3;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Map<Integer, String> map = new HashMap<Integer, String>();

		map.put(10, "Sruthi");
		map.put(20, "Adarsh");
		map.put(30, "Saran");
		map.put(40, "Kiran");

		// print the hashCode of the hashMap object

		System.out.println(map.size());
		// print the size of the map

		System.out.println(map);
		System.out.println(map.get(30));
		// print only the values of the hashMap
		System.out.println(map.values());

		// check if a particular value is present
		System.out.println(map.get("Kiran"));
		System.out.println(map.keySet());
		System.out.println(map.remove(40));

		Set<Integer> set = map.keySet();
		Iterator<Integer> it = set.iterator();
		// check if a particular key is present

		for (Integer k : set) {

			System.out.println(k + " " + map.get(k));
		}

	}

}
