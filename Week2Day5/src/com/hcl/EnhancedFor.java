package com.hcl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class EnhancedFor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> list = new ArrayList<String>();
		list.add("Nishi");
		list.add("Mani");
		list.add("Naidu");
		list.add("Mouni");
		
		for( String s : list) {
			System.out.println(s);
		}
		Set<String> hset = new HashSet<String>();
		hset.add("Ravi");
		hset.add("Sai");
		hset.add("Saran");
		hset.add("Kiran");
		for(String t : hset) {
			System.out.println(t);
		}
		
		  Map<String, Integer> map = new LinkedHashMap<>(); 
		  map.put("Lavanya", 104);
		  map.put("Sravani", 102); 
		  map.put("Roshini", 105);
		  System.out.println(map);

	}

}
