package com.hcl;

import java.util.ArrayList;
import java.util.Arrays;

public class CustomException extends Exception {

	public CustomException(String message) {
		// call the constructor of Exception class
		super(message);
	}
}

class Main {

		  ArrayList<String> CheckFruits = new ArrayList<>(Arrays.asList("Apple", "Mango", "Orange"));

		 
		  public void checkFruite(String Fruit) throws CustomException {

		   
		    if(CheckFruits.contains(Fruit)) {
		      throw new CustomException(Fruit + " already exists");
		    }
		    else {
		     
		      CheckFruits.add(Fruit);
		      System.out.println(Fruit + " is added to the ArrayList");
		    }
		  }

		  public static void main(String[] args) {

		    
		    Main obj = new Main();

		    try {
		      obj.checkFruite("Pinapple");
		      obj.checkFruite("Apple");
		    }

		    catch(CustomException e) {
		      System.out.println("[" + e + "] Exception Occured");
		    }
		  }

}


