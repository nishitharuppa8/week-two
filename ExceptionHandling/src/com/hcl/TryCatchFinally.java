package com.hcl;

import java.util.Scanner;

public class TryCatchFinally {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int result = 0;
		Scanner n = new Scanner(System.in);

		System.out.println("Enter 2 integers");
		int n1 = n.nextInt();
		int n2 = n.nextInt();
		try {

			try {
				result = div(n1, n2);

			} catch (ArithmeticException exp) {

				System.err.println("Div 2 integers");

				try {
					result = result / 0;
				} catch (Exception exp1) {
					System.err.println("inner-inner catch block");
				}
			}

		} catch (ArithmeticException exp1) {

			System.err.println("Outter try-catch");
		}

		finally {
			System.out.println("finally");

			n.close();

		}

		System.out.println(result);

	}

	public static int div(int p, int q) throws ArithmeticException {

		return p / q;

	}

}
