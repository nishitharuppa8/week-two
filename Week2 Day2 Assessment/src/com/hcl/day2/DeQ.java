package com.hcl.day2;

import java.util.ArrayDeque;
import java.util.Deque;

public class DeQ {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Deque<Integer> deque = new ArrayDeque<Integer>();
		deque.add(65);
		deque.add(13);
		deque.add(55);
		deque.add(30);
		
		System.out.println(deque);
		deque.addFirst(50);
		System.out.println(deque);
		deque.addLast(80);
		System.out.println((deque));
		deque.clear();
		System.out.println(deque);
		
		
	}

}
