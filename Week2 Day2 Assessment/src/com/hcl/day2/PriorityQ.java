package com.hcl.day2;

import java.util.PriorityQueue;

public class PriorityQ {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PriorityQueue<Integer> mn = new PriorityQueue<Integer>();
		
		mn.add(5);
		mn.add(3);
		mn.add(7);
		mn.add(33);
		mn.add(66);
		mn.add(44);
		
		
		System.out.println(mn);
		System.out.println(mn.peek());
		
		System.out.println(mn.size());
		
		System.out.println(mn.element());
		
		mn.remove(66);
		
		System.out.println(mn);
		
		mn.removeAll(mn);
		System.out.println(mn.isEmpty());
		
		
	}

}
