package com.hcl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Driver {

	static Map<Integer, UserDef> userMap;

	public Driver() {

		System.out.println("Driver() constructor...");

	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		boolean flag = true;

		while (flag) {

			System.out.println("");
			System.out.println("Display all User");
			System.out.println("To Display by Id");
			System.out.println("To Delete by Id");
			System.out.println("To Exit");

			int k = scanner.nextInt();

			switch (k) {
			case 1:

				Collection<UserDef> list = userMap.values();

				for (UserDef user : list) {

					System.out.println(user);

				}

				break;
			case 2:

				System.out.println("Enter User Id");
				int uid = scanner.nextInt();

				int value = Driver.findUserById(uid);

				if (value > 0) {

					System.out.println("Record Found!");

				}

				else {

					try {
						throw new UserNotFoundException();

					} catch (UserNotFoundException e) {

						System.err.println("User Not Found with Uid " + uid);

					}

				}

				break;

			case 3:

				System.out.println("Enter User Id");
				int id = scanner.nextInt();

				Set<Integer> set = userMap.keySet();

				Iterator<Integer> iterator = set.iterator();

				while (iterator.hasNext()) {
					Integer d = iterator.next();

					if (id == d) {

						System.out.println(userMap.get(d));

						userMap.remove(d);

						System.out.println("This Record is deleted from Map");

						break;

					}

				}

				break;

			case 4:

				flag = false; 
				System.out.println("Welcome");

				break;

			default:
				break;
			}

		}

	}

	static {

		userMap = new HashMap<Integer, UserDef>();

		userMap.put(001, new UserDef(001, "NIshi", "India"));
		userMap.put(002, new UserDef(002, "David", "Australia"));
		userMap.put(003, new UserDef(003, "Gayle", "London"));
		userMap.put(004, new UserDef(004, "Maha", "Punjab"));
		userMap.put(005, new UserDef(005, "Pooja", "West Bengal"));

	}

	public static int findUserById(int uid) {

		int data = 0;

		Set<Integer> keySet = userMap.keySet();

		Iterator<Integer> it = keySet.iterator();

		while (it.hasNext()) {
			Integer n = it.next();

			if (uid == n) {

				System.out.println(userMap.get(n));

				data = n;

			}

		}

		return data;

	}

}
