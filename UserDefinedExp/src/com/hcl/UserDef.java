package com.hcl;

public class UserDef {

	// TODO Auto-generated method stub

	private int id;
	private String name;
	private String city;

	public UserDef(int id, String name, String city) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", city=" + city + "]";
	}

}
