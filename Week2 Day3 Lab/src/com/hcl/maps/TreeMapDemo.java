package com.hcl.maps;

import java.util.Map;
import java.util.TreeMap;

public class TreeMapDemo {

	public static void main(String[] args) {


		
Map<Student,String> map =	new  TreeMap<Student,String>(new MyComparator());
		
			map.put(new Student(101,"Nishi") , "Laptop");
			map.put(new Student(103, "Mani"), "Ipad");
			map.put(new Student(104,"Sai"), "Headsets");
			map.put(new Student(105, "Tarun"), "Airpods");
			map.put(new Student(102,"Vidya"), "Mobile");
			
		
			System.out.println(map);
		
	}

}
