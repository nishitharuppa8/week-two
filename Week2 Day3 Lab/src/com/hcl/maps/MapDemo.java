package com.hcl.maps;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapDemo {

	public static void main(String[] args) {


			Map<Integer,String> map =	new  Hashtable<Integer,String>();
					// Hashset (keys) , List (Values)
				
		
		
			map.put(10, "Sruthi");
			map.put(20, "Adarsh");
			map.put(30, "Saran");
			map.put(40,"Kiran");
			
			
			System.out.println(map);
			
			System.out.println(map.get(30));
			
			System.out.println(map.getOrDefault(1, "Nihal"));
			
			System.out.println(map.keySet());
			System.out.println(map.values());
			
				Set<Integer> set =	map.keySet();
				
				
				Iterator<Integer> it =set.iterator();
				
				while (it.hasNext()) {
				
					
					Integer key = it.next();
					
					System.out.println(key + " "+ map.get(key));
					
				}
				
				
				for (Integer k : set) {
					
					
					System.out.println(k +" "+map.get(k));
				}
				
			
			
		
	}

}
